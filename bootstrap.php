<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/polyfill-str-longest-common-substring library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Polyfill\StrLongestCommonSubstring;

if(!\function_exists('str_longest_common_substring'))
{
	
	/**
	 * Calculates the longest common substring between two strings. The longest
	 * common substring is the longest substring in one piece that is present
	 * inside both of the strings (not to be confused with the longest common
	 * subsequence which aggregate all the common substrings).
	 * 
	 * @param ?string $str1 One of the strings being evaluated for lcs
	 * @param ?string $str2 One of the strings being evaluated for lcs
	 * @return string the longest common substring between the two arguments
	 */
	function str_longest_common_substring(?string $str1, ?string $str2) : string
	{
		return StrLongestCommonSubstring::strLongestCommonSubstring($str1, $str2);
	}
	
}
