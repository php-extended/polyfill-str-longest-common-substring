# php-extended/polyfill-str-longest-common-substring

A php implementation of the longest common substring algorithm.

![coverage](https://gitlab.com/php-extended/polyfill-str-longest-common-substring/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/polyfill-str-longest-common-substring/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/polyfill-str-longest-common-substring": "^1"`


## Basic Usage

This library gives the function `strLongestCommonSubstring(?string $str1, ?string $str2) : string` :

```php

$lcs = \strLongestCommonSubstring('foobarbaz', 'quxfoobar');
// $lcs is 'foobar'

```


## License

MIT (See [license file](LICENSE)).
