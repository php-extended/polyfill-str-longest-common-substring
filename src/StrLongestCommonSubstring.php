<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/polyfill-str-longest-common-substring library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Polyfill;

/**
 * StrLongestCommonSubstring class file.
 * 
 * This class encapsulate the longest common substring function.
 * 
 * @author Anastaszor
 */
final class StrLongestCommonSubstring
{
	
	/**
	 * Returns the longest common substring between the two strings. A null or
	 * empty string is treated as the same as a string of length zero.
	 * 
	 * @param ?string $str1
	 * @param ?string $str2
	 * @return string
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public static function strLongestCommonSubstring(?string $str1, ?string $str2) : string
	{
		if($str1 === $str2)
		{
			return (string) $str1;
		}
		
		if(null === $str1 || '' === $str1 || null === $str2 || '' === $str2)
		{
			return '';
		}
		
		$len1 = (int) \max(0, \mb_strlen($str1, '8bit'));
		$len2 = (int) \max(0, \mb_strlen($str2, '8bit'));
		
		$return = '';
		
		// from https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Longest_common_substring#PHP
		// initialize the LCS array to assume there are no similarities
		$lcs = \array_fill(0, $len1, \array_fill(0, $len2, 0));
		
		$largestSize = 0;
		
		for($i = 0; $i < $len1; $i++)
		{
			for($j = 0; $j < $len2; $j++)
			{
				// check every combination of characters
				if($str1[$i] === $str2[$j])
				{
					// it's the first character, so it's clearly only 1 character long
					$sum = 1;
					if(0 < $i && 0 < $j)
					{
						// it's one character longer than the string from the previous character
						$sum = $lcs[$i - 1][$j - 1] + 1;
					}
					$lcs[$i][$j] = $sum;
					
					if($lcs[$i][$j] > $largestSize)
					{
						// remember this as the largest
						$largestSize = $lcs[$i][$j];
						// wipe any previous results
						$return = '';
						// and then fall through to remember this new value
					}
					
					if($lcs[$i][$j] === $largestSize)
					{
						// remember the largest string(s)
						$return = (string) \mb_substr($str1, $i - $largestSize + 1, $largestSize, '8bit');
					}
				}
				// else, $lcs should be set to 0, which it was already initialized to
			}
		}
		
		// return the list of matches
		return $return;
	}
	
}
