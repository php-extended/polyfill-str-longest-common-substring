<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/polyfill-str-longest-common-substring library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Polyfill\StrLongestCommonSubstring;
use PHPUnit\Framework\TestCase;

/**
 * StrLongestCommonSubstringTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Polyfill\StrLongestCommonSubstring
 *
 * @internal
 *
 * @small
 */
class StrLongestCommonSubstringTest extends TestCase
{	
	/**
	 * The object to test.
	 * 
	 * @var StrLongestCommonSubstring
	 */
	protected $_object;
	
	public function testStrLongestCommonSubstringNull() : void
	{
		$this->assertEquals('', $this->_object->strLongestCommonSubstring(null, null));
	}
	
	public function testStrLongestCommonSubstringEmpty() : void
	{
		$this->assertEquals('', $this->_object->strLongestCommonSubstring('', 'toto'));
	}
	
	public function testStrLongestCommonSubstringUnique() : void
	{
		$this->assertEquals('foo', $this->_object->strLongestCommonSubstring('ffoobaz', 'barfooqux'));
	}
	
	public function testStrLongestCommonSubstringNative() : void
	{
		$this->assertEquals('foobar', \str_longest_common_substring('sfoobar', 'foobars'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new StrLongestCommonSubstring();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}
